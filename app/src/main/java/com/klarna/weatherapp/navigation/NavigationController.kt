package com.klarna.weatherapp.navigation

import com.klarna.weatherapp.R
import com.klarna.weatherapp.WeatherActivity
import com.klarna.weatherapp.screen.home.HomeFragment

class NavigationController(mWeatherActivity: WeatherActivity) {

    private val mContainerId = R.id.fragment_container
    private val mFragmentManager = mWeatherActivity.supportFragmentManager

    fun navigateToHome() {
        mFragmentManager
            .beginTransaction()
            .replace(mContainerId,HomeFragment())
            .commitAllowingStateLoss()
    }
}