package com.klarna.weatherapp.util

import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import androidx.core.content.ContextCompat
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun <T>Flowable<T>.observe(onNext: (T) -> Unit, onError: ((Throwable) -> Unit)? = null, onComplete: (() -> Unit)? = null) : Disposable{
       return this.subscribeOn(Schedulers.newThread())
           .observeOn(Schedulers.io())
           .subscribe ({
                onNext(it)
           },{
                onError?.invoke(it)
           },{
               onComplete?.invoke()
           })
}

const val LOCATION_REQUEST_CODE = 1111

fun Context.isPermissionGranted(permission: String): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    } else {
        ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
    }
}

fun Context.isConnectedToNetwork(): Boolean {
    try {
        val connectivityManager = applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val activeNetwork = connectivityManager.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        }
        return false
    } catch (e: Exception) {
        return false
    }
}
