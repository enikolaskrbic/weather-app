package com.klarna.weatherapp.util

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtil {

    private val simpleDateFormat = SimpleDateFormat("HH:mm")

    fun formatTime(date: Date): String{
        return simpleDateFormat.format(date)
    }
}