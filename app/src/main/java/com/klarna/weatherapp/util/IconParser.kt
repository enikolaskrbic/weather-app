package com.klarna.weatherapp.util

import com.klarna.weatherapp.R

object IconParser {

    private val CLEAR_DAY = "clear-day"
    private val CLEAR_NIGHT = "clear-night"
    private val RAIN = "rain"
    private val SNOW = "snow"
    private val SLEET = "sleet"
    private val WIND = "wind"
    private val FOG = "fog"
    private val CLOUDY = "cloudy"
    private val PARTLY_CLOUDY_DAY = "partly-cloudy-day"
    private val PARTLY_CLOUDY_NIGHT = "partly-cloudy-night"

    fun getIcon(value: String): Int{
        return when(value){
            CLEAR_DAY -> R.drawable.ic_clear_day
            CLEAR_NIGHT -> R.drawable.ic_clear_night
            RAIN -> R.drawable.ic_rain
            SNOW -> R.drawable.ic_snow
            SLEET -> R.drawable.ic_sleet
            WIND -> R.drawable.ic_wind
            FOG -> R.drawable.ic_fog
            CLOUDY -> R.drawable.ic_cloud
            PARTLY_CLOUDY_DAY -> R.drawable.ic_partly_cloud_day
            PARTLY_CLOUDY_NIGHT -> R.drawable.ic_partly_cloud_night
            else -> R.drawable.ic_clear_day
        }
    }
}