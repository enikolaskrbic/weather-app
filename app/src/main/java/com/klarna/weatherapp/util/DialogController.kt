package com.klarna.weatherapp.util

import android.app.Activity
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.klarna.weatherapp.R

object DialogController {

    fun openLocationDialog(location : (lat: Double, lng: Double) -> Unit, activity: Activity){
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Add location")
        builder.setView(R.layout.dialog_location)
        val dialog = builder.show()
        val lat = dialog.findViewById<EditText>(R.id.edit_text_lat)
        val lng = dialog.findViewById<EditText>(R.id.edit_text_lng)
        val okButton = dialog.findViewById<Button>(R.id.button_ok)
        okButton?.setOnClickListener {
            location.invoke(lat?.text.toString().toDouble(), lng?.text.toString().toDouble())
        }

    }
}