package com.klarna.weatherapp.services

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.location.Location
import android.location.LocationManager
import com.klarna.weatherapp.WeatherApplication
import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.data.event.SuccessEvent
import com.klarna.weatherapp.util.Singleton
import com.klarna.weatherapp.util.isPermissionGranted
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred




class LocationService {
    companion object : Singleton<LocationService>(::LocationService)

    private val mLocationManager: LocationManager
        get() = WeatherApplication.getLocationManager()

    private val application: Application
        get() = WeatherApplication.getApplication()

    @SuppressLint("MissingPermission")
    fun getLocation(): Deferred<Event<Location>>{
        val completableDeferredLocation = CompletableDeferred<Event<Location>>()
        if(!isPermissionGranted()){
            completableDeferredLocation.complete(SuccessEvent(getDefaultLocation()))
            return completableDeferredLocation
        }
        val location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        if(location == null){
            completableDeferredLocation.complete(SuccessEvent(getDefaultLocation()))
        }else{
            completableDeferredLocation.complete(SuccessEvent(location))
        }
        return completableDeferredLocation
    }

    private fun getDefaultLocation(): Location{
        val defaultLocation = Location("")
        defaultLocation.latitude = 59.337239
        defaultLocation.longitude = 18.062381
        return defaultLocation
    }

    private fun  isPermissionGranted(): Boolean{
        return application.isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION) && application.isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION)
    }
}