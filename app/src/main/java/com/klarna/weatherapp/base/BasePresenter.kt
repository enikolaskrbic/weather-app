package com.klarna.weatherapp.base

/**
 *
 * This interface is responsible for UI commands and linked to use cases to execute business logic
 *
 */
interface BasePresenter {

    fun subscribe(view: BaseView)

    fun unsubscribe()
}