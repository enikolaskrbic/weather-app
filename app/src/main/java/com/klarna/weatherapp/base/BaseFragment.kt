package com.klarna.weatherapp.base

import android.Manifest
import android.os.Build
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.klarna.weatherapp.R
import com.klarna.weatherapp.error.*
import com.klarna.weatherapp.util.LOCATION_REQUEST_CODE
import com.klarna.weatherapp.util.isConnectedToNetwork
import com.klarna.weatherapp.util.isPermissionGranted

open class BaseFragment:  Fragment() {

    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showToast(resourceId: Int) {
        showToast(getString(resourceId))
    }

    open fun showBaseError(weatherError: WeatherError) {
        when(weatherError){
            is BackendError -> {
                showToast(getString(R.string.message_cant_fetch_data))
            }

            is ThrowableError -> {
                context?.let {
                    if(!it.isConnectedToNetwork()){
                        showToast(getString(R.string.error_please_check_internet_connection))
                        return@let
                    }
                    showToast(weatherError.throwable.localizedMessage)
                }
            }

            is DatabaseSaveError -> {
                showToast(getString(R.string.message_cant_save_data))
            }

            is UnknownError -> {
                showToast(getString(R.string.error_unknown))
            }
        }
    }

    fun requestLocationPermissions(): Boolean {
        activity?.let {
            val permissions = ArrayList<String>()
            if (!it.isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
            }
            if (!it.isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
            }

            return if (permissions.size > 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissions.toTypedArray(), LOCATION_REQUEST_CODE)
                }
                false
            } else {
                true
            }
        }
        return false
    }
}