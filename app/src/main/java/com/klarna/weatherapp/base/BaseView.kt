package com.klarna.weatherapp.base

import com.klarna.weatherapp.error.WeatherError

interface BaseView {
    fun showProgress()
    fun hideProgress()
    fun showError(weatherError: WeatherError)
}