package com.klarna.weatherapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.klarna.weatherapp.R
import com.klarna.weatherapp.data.repository.model.ForecastItemModel
import com.klarna.weatherapp.util.DateTimeUtil
import com.klarna.weatherapp.util.IconParser
import kotlinx.android.synthetic.main.item_hourly.view.*
import java.util.*

class HourlyAdapter:  ListAdapter<ForecastItemModel, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ForecastItemModel>() {
            override fun areItemsTheSame(oldItem: ForecastItemModel, newItem: ForecastItemModel): Boolean {
                return oldItem.time == newItem.time
            }

            override fun areContentsTheSame(oldItem: ForecastItemModel, newItem: ForecastItemModel): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return HourlyHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_hourly, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            val historyOrderHolder = holder as HourlyHolder
            historyOrderHolder.bindData(it, position)
        }
    }

    inner class HourlyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var mPosition: Int = 0

        fun bindData(forecastItemModel: ForecastItemModel, position: Int) {
            mPosition = position
            itemView.text_view_hourly_time.text = DateTimeUtil.formatTime(Date(forecastItemModel.time*1000))
            itemView.text_view_hourly_temperature.text = "%.0f".format(forecastItemModel.temperature) +"°f"
            itemView.image_view_hourly_icon?.setImageResource(IconParser.getIcon(forecastItemModel.icon))
        }

    }
}