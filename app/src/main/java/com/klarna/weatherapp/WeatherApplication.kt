package com.klarna.weatherapp

import android.app.Application
import android.location.LocationManager
import io.realm.Realm
import io.realm.RealmConfiguration



class WeatherApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initRealm()
        application = this
    }

    private fun initRealm(){
        Realm.init(this)
        val mRealmConfiguration = RealmConfiguration.Builder()
            .name("klarnaweather.realm")
            .schemaVersion(1)
            .deleteRealmIfMigrationNeeded()
            .build()

        Realm.getInstance(mRealmConfiguration)
        Realm.setDefaultConfiguration(mRealmConfiguration)
    }

    companion object {
        private lateinit var application: Application
        private lateinit var locationManager: LocationManager

        fun getApplication(): Application {
            return application
        }

        fun getLocationManager(): LocationManager {
            return application.getSystemService(LOCATION_SERVICE) as LocationManager
        }
    }
}