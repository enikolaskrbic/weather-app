package com.klarna.weatherapp.error

sealed class WeatherError
//GLOBAL ERROR
object UnknownError : WeatherError()
//DATABASE ERROR
object DatabaseSaveError: WeatherError()
object DatabaseError: WeatherError()
//API ERROR
data class ThrowableError(val throwable: Throwable) : WeatherError()
data class ErrorResponse(val code: Int, val error: String) : WeatherError()

object BackendError : WeatherError()

