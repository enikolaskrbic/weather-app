package com.klarna.weatherapp.screen.home

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.klarna.weatherapp.R
import com.klarna.weatherapp.adapter.HourlyAdapter
import com.klarna.weatherapp.base.BaseFragment
import com.klarna.weatherapp.data.event.ErrorEvent
import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.data.event.SuccessEvent
import com.klarna.weatherapp.data.repository.model.ForecastModel
import com.klarna.weatherapp.error.WeatherError
import com.klarna.weatherapp.util.DialogController
import com.klarna.weatherapp.util.IconParser
import com.klarna.weatherapp.util.LOCATION_REQUEST_CODE
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment: BaseFragment(),HomeContract.HomeView {



    private lateinit var mHomePresenter: HomePresenter

    private val mHourlyAdapter = HourlyAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mHomePresenter = HomePresenter()
        initRecyclerViews()
        requestLocationPermissions()
        initListener()
    }

    private fun initListener() {
        button_add_location.setOnClickListener {
            activity?.run {
                DialogController.openLocationDialog({lat, lng ->
                    mHomePresenter.fetchWeatherFromLocation(lat,lng)
                }, this)
            }

        }
    }

    override fun onStart() {
        super.onStart()
        mHomePresenter.subscribe(this)
    }

    override fun onStop() {
        super.onStop()
        mHomePresenter.unsubscribe()
    }

    override fun updateWeather(forecastModel: Event<List<ForecastModel?>>) {
        activity?.runOnUiThread {
            when(forecastModel){
                is SuccessEvent -> {
                    forecastModel.data?.run {
                        if(size > 0)
                            populateForecastModel(this[0])
                    }
                }
                is ErrorEvent -> {
                    showError(forecastModel.error)
                }
            }
        }
    }

    override fun showProgress() {
        activity?.runOnUiThread {
            dots_grow_load_weather?.visibility = View.VISIBLE
        }
    }

    override fun hideProgress() {
        activity?.runOnUiThread {
            dots_grow_load_weather?.visibility = View.GONE
        }
    }

    override fun showError(weatherError: WeatherError) {
        activity?.runOnUiThread {
            showBaseError(weatherError)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            LOCATION_REQUEST_CODE -> {
                if(grantResults.isNotEmpty()){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        mHomePresenter.fetchWeather()
                }
            }
        }

    }

    private fun populateForecastModel(forecastModel: ForecastModel?) {
        forecastModel?.run {
            text_view_place?.text = timezone
            currently?.run {
                text_view_temperature?.text = "%.0f".format(temperature)
                image_view_main_icon?.setImageResource(IconParser.getIcon(icon))
                text_view_summary?.text = summary
                //FIRST ROW
                text_view_humidity?.text ="%.0f".format(humidity*100)
                text_view_feels_like?.text ="%.0f".format(apparentTemperature) +"°f"
                text_view_wind?.text = "%.0f".format(windSpeed) +" mph"
                text_view_dew_point?.text = "%.0f".format(dewPoint) +"°"
                //SECOND ROW
                text_view_pressure?.text = "%.0f".format(pressure) +" hPa"
                text_view_visibility?.text = "%.1f".format(visibility) +" km/h"
                text_view_chance_of_rain?.text = "%.1f".format(precipProbability*100) +"%"
                text_view_uv_index?.text = "%.0f".format(uvIndex)

            }
            hourly?.run {
                mHourlyAdapter.submitList(data)
            }

        }
    }

    private fun initRecyclerViews() {
        val layoutManagerDeliveryCategories = LinearLayoutManager(context)
        layoutManagerDeliveryCategories.orientation = LinearLayoutManager.HORIZONTAL
        recycler_view_hourly?.layoutManager = layoutManagerDeliveryCategories
        recycler_view_hourly?.setHasFixedSize(false)
        recycler_view_hourly?.adapter = mHourlyAdapter
    }
}