package com.klarna.weatherapp.screen.home

import com.klarna.weatherapp.base.BasePresenter
import com.klarna.weatherapp.base.BaseView
import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.data.repository.model.ForecastModel

interface HomeContract {

    interface HomeView : BaseView {
        fun updateWeather(forecastModel: Event<List<ForecastModel?>>)
    }

    interface HomePresenter : BasePresenter {
        fun fetchWeather()
        fun fetchWeatherFromLocation(lat: Double, lng: Double)
    }
}