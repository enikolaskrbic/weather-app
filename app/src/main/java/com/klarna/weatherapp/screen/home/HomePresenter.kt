package com.klarna.weatherapp.screen.home

import com.klarna.weatherapp.base.BaseView
import com.klarna.weatherapp.data.event.ErrorEvent
import com.klarna.weatherapp.data.event.SuccessEvent
import com.klarna.weatherapp.data.usecase.FetchAndSaveWeatherUseCase
import com.klarna.weatherapp.data.usecase.GetLocationUseCase
import com.klarna.weatherapp.data.usecase.GetWeatherUseCase
import com.klarna.weatherapp.data.usecase.value.PlaceForecastValue
import com.klarna.weatherapp.util.observe
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

class HomePresenter : HomeContract.HomePresenter {
    override fun fetchWeatherFromLocation(lat: Double, lng: Double) {
        GlobalScope.launch {
            val fetchResult = mFetchAndSaveWeatherUseCase.invoke(PlaceForecastValue(lat, lng)).await()
            if(fetchResult is ErrorEvent) mHomeView.get()?.showError(fetchResult.error)
            mHomeView.get()?.hideProgress()
        }
    }

    private var mHomeView = WeakReference<HomeContract.HomeView>(null)

    private val mFetchAndSaveWeatherUseCase: FetchAndSaveWeatherUseCase by lazy {
        FetchAndSaveWeatherUseCase()
    }

    private val mGetLocationUseCase: GetLocationUseCase by lazy {
        GetLocationUseCase()
    }

    private val mGetWeatherUseCase : GetWeatherUseCase by lazy {
        GetWeatherUseCase()
    }

    private var disposableCities: Disposable? = null

    override fun fetchWeather() {
        GlobalScope.launch {
            mHomeView.get()?.showProgress()
            val location = mGetLocationUseCase.invoke(Unit).await()
            if(location is SuccessEvent){
                location.data?.run {
                    val fetchResult = mFetchAndSaveWeatherUseCase.invoke(PlaceForecastValue(latitude, longitude)).await()
                    if(fetchResult is ErrorEvent) mHomeView.get()?.showError(fetchResult.error)
                    mHomeView.get()?.hideProgress()
                }
            }
        }
    }

    override fun subscribe(view: BaseView) {
        mHomeView = WeakReference(view as HomeContract.HomeView)
        fetchWeather()
        mGetWeatherUseCase.invoke(Unit).observe({
            mHomeView.get()?.updateWeather(it)
        })

    }

    override fun unsubscribe() {
        mHomeView = WeakReference<HomeContract.HomeView>(null)
        disposableCities?.dispose()
    }
}