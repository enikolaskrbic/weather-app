package com.klarna.weatherapp.data.database.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class CityRealmModel(
    @PrimaryKey
    var id: Long = 1L,
    var name: String = "",
    var latitude: Double = 1.0,
    var longitude: Double = 1.0,
    var forecast: ForecastRealmModel? = null
) : RealmObject() {
    companion object {
        const val ID = "id"
    }
}





