package com.klarna.weatherapp.data.repository.model

data class ForecastModel(
    val latitude: Double,
    val longitude: Double,
    val timezone: String,
    val currently: ForecastItemModel?,
    val hourly: HourlyModel?
)



