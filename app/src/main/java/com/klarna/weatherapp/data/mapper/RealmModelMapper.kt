package com.klarna.weatherapp.data.mapper

import com.klarna.weatherapp.data.database.model.ForecastItemRealmModel
import com.klarna.weatherapp.data.database.model.ForecastRealmModel
import com.klarna.weatherapp.data.database.model.HourlyRealmModel
import com.klarna.weatherapp.data.repository.model.ForecastItemModel
import com.klarna.weatherapp.data.repository.model.ForecastModel
import com.klarna.weatherapp.data.repository.model.HourlyModel

object RealmModelMapper {

    fun mapForecast(forecastRealmModel: ForecastRealmModel?): ForecastModel? {
        forecastRealmModel?.run {
           return ForecastModel(
                forecastRealmModel.latitude,
                forecastRealmModel.longitude,
                forecastRealmModel.timezone,
                mapForecastItem(forecastRealmModel.currently),
                mapHourly(forecastRealmModel.hourly))
        }
        return null
    }

    fun mapForecastItem(forecastItemRealmModel: ForecastItemRealmModel?): ForecastItemModel?{
        if(forecastItemRealmModel == null) return null
        return ForecastItemModel(
            forecastItemRealmModel.time,
            forecastItemRealmModel.summary,
            forecastItemRealmModel.icon,
            forecastItemRealmModel.precipIntensity,
            forecastItemRealmModel.precipProbability,
            forecastItemRealmModel.temperature,
            forecastItemRealmModel.apparentTemperature,
            forecastItemRealmModel.dewPoint,
            forecastItemRealmModel.humidity,
            forecastItemRealmModel.pressure,
            forecastItemRealmModel.windSpeed,
            forecastItemRealmModel.windGust,
            forecastItemRealmModel.windBearing,
            forecastItemRealmModel.cloudCover,
            forecastItemRealmModel.uvIndex,
            forecastItemRealmModel.visibility,
            forecastItemRealmModel.ozone
        )
    }

    fun mapHourly(hourlyRealmModel: HourlyRealmModel?): HourlyModel?{
        if(hourlyRealmModel == null) return null
        return HourlyModel(
            hourlyRealmModel.summary,
            hourlyRealmModel.icon,
            hourlyRealmModel.data.map { mapForecastItem(it) }
        )
    }
}