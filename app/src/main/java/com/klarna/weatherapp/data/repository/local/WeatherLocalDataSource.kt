package com.klarna.weatherapp.data.repository.local

import android.os.Handler
import android.os.Looper
import com.klarna.weatherapp.data.database.model.ForecastRealmModel
import com.klarna.weatherapp.data.event.ErrorEvent
import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.data.event.SuccessEvent
import com.klarna.weatherapp.data.mapper.ModelRealmMapper
import com.klarna.weatherapp.data.mapper.RealmModelMapper
import com.klarna.weatherapp.data.repository.WeatherDataSource
import com.klarna.weatherapp.data.repository.model.ForecastModel
import com.klarna.weatherapp.data.usecase.value.ForecastValue
import com.klarna.weatherapp.data.usecase.value.PlaceForecastValue
import com.klarna.weatherapp.error.DatabaseError
import com.klarna.weatherapp.error.DatabaseSaveError
import com.klarna.weatherapp.util.Singleton
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred

class WeatherLocalDataSource : WeatherDataSource {

    companion object : Singleton<WeatherLocalDataSource>(::WeatherLocalDataSource)

    private var mFlowableEmitterWeather: FlowableEmitter<Event<List<ForecastModel?>>>? = null

    override fun saveWeather(forecastValue: ForecastValue): Deferred<Event<Unit>> {
        addForecastListener()
        val completableDeferred = CompletableDeferred<Event<Unit>>()
        try {
            Realm.getDefaultInstance().use { realmInstance ->
                realmInstance.executeTransactionAsync { realm ->
                    realm.where(ForecastRealmModel::class.java).findAll().deleteAllFromRealm()
                    realm.copyToRealm(ModelRealmMapper.mapForecast(forecastValue.forecastModel))
                }
                completableDeferred.complete(SuccessEvent(Unit))
            }
        } catch (e: Throwable) {
            completableDeferred.complete(ErrorEvent(DatabaseSaveError))
        }
        return completableDeferred
    }

    override fun getWeather(): Flowable<Event<List<ForecastModel?>>> {
        addForecastListener()
        return Flowable.create({
            mFlowableEmitterWeather = it
            try {
                Realm.getDefaultInstance().use { realmInstance ->
                    realmInstance.executeTransaction { realm ->
                        val forecastResults =
                            realm.where(ForecastRealmModel::class.java).findAll() as List<ForecastRealmModel>
                        mFlowableEmitterWeather?.onNext(SuccessEvent(forecastResults.map {
                            RealmModelMapper.mapForecast(
                                it
                            )
                        }))
                    }
                }
            } catch (e: Exception) {
                mFlowableEmitterWeather?.onNext(ErrorEvent(DatabaseError))
            }
        }, BackpressureStrategy.LATEST)
    }

    override fun fetchWeather(placeForecastValue: PlaceForecastValue): Deferred<Event<ForecastModel>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun addForecastListener() {
        val mainHandler = Handler(Looper.getMainLooper())
        val myRunnable = Runnable {
            val realm = Realm.getDefaultInstance()
            val citiesResults = realm.where(ForecastRealmModel::class.java).findAll()
            citiesResults?.removeAllChangeListeners()
            citiesResults?.addChangeListener(mForecastChangeListener)
            realm.close()
        }
        mainHandler.post(myRunnable)
    }

    private
    var mForecastChangeListener: RealmChangeListener<RealmResults<ForecastRealmModel>> =
        RealmChangeListener { element ->
            val resultsForecast = element as List<ForecastRealmModel>
            mFlowableEmitterWeather?.onNext(SuccessEvent(resultsForecast.map { RealmModelMapper.mapForecast(it) }))
        }
}