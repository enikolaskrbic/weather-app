package com.klarna.weatherapp.data.repository

import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.data.repository.model.ForecastModel
import com.klarna.weatherapp.data.usecase.value.ForecastValue
import com.klarna.weatherapp.data.usecase.value.PlaceForecastValue
import io.reactivex.Flowable
import kotlinx.coroutines.Deferred

interface WeatherDataSource {

    fun fetchWeather(placeForecastValue: PlaceForecastValue) : Deferred<Event<ForecastModel>>

    fun saveWeather(forecastValue: ForecastValue): Deferred<Event<Unit>>

    fun getWeather(): Flowable<Event<List<ForecastModel?>>>
}