package com.klarna.weatherapp.data.usecase.value

import com.klarna.weatherapp.data.repository.model.ForecastModel

data class ForecastValue(
    val forecastModel: ForecastModel
)