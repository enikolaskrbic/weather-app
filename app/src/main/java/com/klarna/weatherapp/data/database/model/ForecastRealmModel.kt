package com.klarna.weatherapp.data.database.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class ForecastRealmModel(
    @PrimaryKey
    var id: Long = 1L,
    var latitude: Double = 1.0,
    var longitude: Double = 1.0,
    var timezone: String = "",
    var currently: ForecastItemRealmModel? = null,
    var hourly: HourlyRealmModel? = null
) : RealmObject()