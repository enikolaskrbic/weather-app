package com.klarna.weatherapp.data.repository

import android.app.Application
import com.klarna.weatherapp.WeatherApplication
import com.klarna.weatherapp.data.api.RestApi
import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.data.repository.local.WeatherLocalDataSource
import com.klarna.weatherapp.data.repository.model.ForecastModel
import com.klarna.weatherapp.data.repository.remote.WeatherRemoteDataSource
import com.klarna.weatherapp.data.usecase.value.ForecastValue
import com.klarna.weatherapp.data.usecase.value.PlaceForecastValue
import com.klarna.weatherapp.util.Singleton
import io.reactivex.Flowable
import kotlinx.coroutines.Deferred

class WeatherRepository : WeatherDataSource {

    companion object : Singleton<WeatherRepository>(::WeatherRepository)

    private val application: Application
        get() = WeatherApplication.getApplication()

    private val weatherRemoteDataSource: WeatherDataSource
        get() = WeatherRemoteDataSource.getInstance(RestApi.getWeatherApi(application))

    private val weatherLocalDataSource: WeatherDataSource
        get() = WeatherLocalDataSource.getInstance()

    override fun fetchWeather(placeForecastValue: PlaceForecastValue): Deferred<Event<ForecastModel>> {
        return weatherRemoteDataSource.fetchWeather(placeForecastValue)
    }

    override fun saveWeather(forecastValue: ForecastValue): Deferred<Event<Unit>> {
       return weatherLocalDataSource.saveWeather(forecastValue)
    }

    override fun getWeather(): Flowable<Event<List<ForecastModel?>>> {
        return weatherLocalDataSource.getWeather()
    }
}