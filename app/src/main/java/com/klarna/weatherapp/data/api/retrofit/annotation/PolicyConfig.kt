package com.klarna.weatherapp.data.api.retrofit.annotation


@MustBeDocumented
@Target(AnnotationTarget.FUNCTION,
        AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER,
        AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class PolicyConfig