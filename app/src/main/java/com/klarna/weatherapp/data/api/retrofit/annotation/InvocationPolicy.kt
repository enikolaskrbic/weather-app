package com.klarna.weatherapp.data.api.retrofit.annotation

@PolicyConfig
interface InvocationPolicy {
    /**
     * The response status codes for which the [EitherCall] attempts to convert to
     * the first parameterized type.
     */
    fun left(): IntArray

    /**
     * The response status codes for which the [EitherCall] attempts to convert to
     * the second parameterized type.
     */
    fun right(): IntArray

    /**
     * Sequential range of response status code values at which the [EitherCall]
     * tries to convert to the first parameterized type.
     */
    fun leftRange(): Array<StatusCodeRange>

    /**
     * Sequential range of response status code values at which the [EitherCall]
     * tries to convert to the second parameterized type.
     */
    fun rightRange(): Array<StatusCodeRange>

    /**
     * Sequential range of values ​​for the status code of the response.
     */
    enum class StatusCodeRange constructor(
            /**
             * The lower bound.
             */
            private val low: Int,
            /**
             * The upper bound.
             */
            private val high: Int) {
        SUCCESS(200, 299),
        REDIRECT(300, 399),
        CLIENT_ERROR(400, 499),
        SERVER_ERROR(500, 599);

        fun low(): Int {
            return low
        }

        fun high(): Int {
            return high
        }

        companion object {

            fun inRange(ranges: Array<StatusCodeRange>, code: Int): Boolean {
                for (range in ranges) {
                    if (inRange(range, code)) {
                        return true
                    }
                }

                return false
            }


            fun inRange(range: StatusCodeRange, code: Int): Boolean {
                return code >= range.low() && code <= range.high()
            }
        }
    }
}