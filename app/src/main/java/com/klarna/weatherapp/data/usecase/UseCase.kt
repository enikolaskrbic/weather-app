package com.klarna.weatherapp.data.usecase

/**
 *
 * This interface is responsible for UI business logic
 *
 */
interface UseCase<T, F> {
    fun invoke(value: T): F
}