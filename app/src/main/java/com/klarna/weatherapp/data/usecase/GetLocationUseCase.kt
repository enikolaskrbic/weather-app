package com.klarna.weatherapp.data.usecase

import android.location.Location
import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.services.LocationService
import kotlinx.coroutines.Deferred

class GetLocationUseCase: UseCase<Unit, Deferred<Event<Location>>> {

    private val mLocationService = LocationService.getInstance()

    override fun invoke(value: Unit): Deferred<Event<Location>> {
        return mLocationService.getLocation()
    }
}