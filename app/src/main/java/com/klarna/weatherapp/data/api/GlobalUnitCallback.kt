package com.klarna.weatherapp.data.api


import com.klarna.weatherapp.data.api.retrofit.EitherCallback
import com.klarna.weatherapp.data.event.ErrorEvent
import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.error.ErrorResponse
import com.klarna.weatherapp.error.ThrowableError
import com.klarna.weatherapp.error.UnknownError
import kotlinx.coroutines.CompletableDeferred

class GlobalUnitCallback<L, R, T>(private val deferred: CompletableDeferred<Event<T>>, val onSuccess: (L?) -> Unit) :
    EitherCallback<L, R> {

    override fun onLeft(left: L) {
        onSuccess(left)
    }

    override fun onRight(right: R) {
        if (right is ErrorResponse) {
            deferred.complete(ErrorEvent(right))
        } else
            deferred.complete(ErrorEvent(UnknownError))
    }

    override fun onException(t: Throwable) {
        deferred.complete(ErrorEvent(ThrowableError(t)))
    }

    override fun onUnauthorized(right: R) {
        //NOTHING AT THE MOMENT -> ALL API IS OPEN
    }
}