package com.klarna.weatherapp.data.api

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.klarna.weatherapp.BuildConfig
import com.klarna.weatherapp.data.api.retrofit.EitherCallAdapterFactory
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RestApi {

    fun getWeatherApi(application: Application): WeatherApi {
        weatherApi?.let {
            return it
        }
        return provideApiServices(
            provideRetrofit(
                BuildConfig.BASE_URL, provideGson(), providesOkHttpClient(
                    provideHttpCatche(application)
                )
            )
        )
    }

    private var weatherApi: WeatherApi? = null

    private fun providesOkHttpClient(cache: Cache): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.connectTimeout(30, TimeUnit.SECONDS)
        clientBuilder.readTimeout(30, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(30, TimeUnit.SECONDS)
        return clientBuilder.build()
    }

    private fun provideHttpCatche(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    private fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    private fun provideApiServices(retrofit: Retrofit): WeatherApi {
        return retrofit.create<WeatherApi>(WeatherApi::class.java)
    }

    private fun provideRetrofit(baseUrl: String, gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(EitherCallAdapterFactory.create())
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .build()
    }

}