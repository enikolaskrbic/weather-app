package com.klarna.weatherapp.data.usecase.value

data class AddCityValue(
    val id: Long,
    val name: String,
    val latitude: Double,
    val longitude: Double
)