package com.klarna.weatherapp.data.mapper

import com.klarna.weatherapp.data.database.model.ForecastItemRealmModel
import com.klarna.weatherapp.data.database.model.ForecastRealmModel
import com.klarna.weatherapp.data.database.model.HourlyRealmModel
import com.klarna.weatherapp.data.repository.model.ForecastItemModel
import com.klarna.weatherapp.data.repository.model.ForecastModel
import com.klarna.weatherapp.data.repository.model.HourlyModel
import io.realm.RealmList

object ModelRealmMapper {

    fun mapForecast(forecastModel: ForecastModel): ForecastRealmModel {
        return ForecastRealmModel(
            forecastModel.currently?.time?:1L,
            forecastModel.latitude,
            forecastModel.longitude,
            forecastModel.timezone,
            mapForecastItem(forecastModel.currently),
            mapHourly(forecastModel.hourly)
        )
    }

    fun mapForecastItem(forecastItemModel: ForecastItemModel?): ForecastItemRealmModel? {
        if(forecastItemModel == null) return null
        return ForecastItemRealmModel(
            forecastItemModel.time,
            forecastItemModel.summary,
            forecastItemModel.icon,
            forecastItemModel.precipIntensity,
            forecastItemModel.precipProbability,
            forecastItemModel.temperature,
            forecastItemModel.apparentTemperature,
            forecastItemModel.dewPoint,
            forecastItemModel.humidity,
            forecastItemModel.pressure,
            forecastItemModel.windSpeed,
            forecastItemModel.windGust,
            forecastItemModel.windBearing,
            forecastItemModel.cloudCover,
            forecastItemModel.uvIndex,
            forecastItemModel.visibility,
            forecastItemModel.ozone
        )
    }

    fun mapHourly(hourlyModel: HourlyModel?): HourlyRealmModel? {
        if(hourlyModel == null) return null
        return HourlyRealmModel(
            hourlyModel.summary,
            hourlyModel.icon,
            mapForecastItemList(hourlyModel.data.map { mapForecastItem(it) })
        )
    }

    private fun mapForecastItemList(list: List<ForecastItemRealmModel?>): RealmList<ForecastItemRealmModel?> {
        val forecastItemRealmList = RealmList<ForecastItemRealmModel?>()
        forecastItemRealmList.addAll(list)
        return forecastItemRealmList
    }
}