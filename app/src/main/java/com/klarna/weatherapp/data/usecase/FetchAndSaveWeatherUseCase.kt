package com.klarna.weatherapp.data.usecase

import com.klarna.weatherapp.data.event.ErrorEvent
import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.data.event.SuccessEvent
import com.klarna.weatherapp.data.repository.WeatherRepository
import com.klarna.weatherapp.data.repository.model.ForecastModel
import com.klarna.weatherapp.data.usecase.value.ForecastValue
import com.klarna.weatherapp.data.usecase.value.PlaceForecastValue
import com.klarna.weatherapp.error.DatabaseSaveError
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FetchAndSaveWeatherUseCase : UseCase<PlaceForecastValue, Deferred<Event<ForecastModel>>> {

    private val mWeatherRepository = WeatherRepository.getInstance()

    override fun invoke(value: PlaceForecastValue): Deferred<Event<ForecastModel>> {
        val completableDeferred = CompletableDeferred<Event<ForecastModel>>()
        GlobalScope.launch {
            val fetchResponse = mWeatherRepository.fetchWeather(value).await()
            when (fetchResponse) {
                is SuccessEvent -> {
                    fetchResponse.data?.let {
                        val saveResponse = mWeatherRepository.saveWeather(ForecastValue(it)).await()
                        when (saveResponse) {
                            is SuccessEvent -> {
                                completableDeferred.complete(fetchResponse)
                            }
                            else -> {
                                completableDeferred.complete(ErrorEvent(DatabaseSaveError))
                            }
                        }
                    }
                }
                else -> {
                    completableDeferred.complete(fetchResponse)
                }
            }
        }

        return completableDeferred
    }
}