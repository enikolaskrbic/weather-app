package com.klarna.weatherapp.data.api.retrofit

import android.os.Handler
import android.os.Looper
import com.klarna.weatherapp.data.api.retrofit.annotation.InvocationPolicy
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class EitherCallAdapterFactory(private val handler: Handler) : CallAdapter.Factory() {
    val ANNOTATIONS = arrayOfNulls<Annotation>(0)


    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        val rawType = CallAdapter.Factory.getRawType(returnType)
        if (EitherCall::class.java != rawType) {
            return null
        }
        if (returnType !is ParameterizedType) {
            throw IllegalStateException("EitherCall return type must be parameterized" + " as EitherCall<Foo> or EitherCall<? extends Foo>")
        }
        val leftType = CallAdapter.Factory.getParameterUpperBound(0, returnType)
        val rightType = CallAdapter.Factory.getParameterUpperBound(1, returnType)

        val left = retrofit.responseBodyConverter<Any>(leftType, ANNOTATIONS)
        val right = retrofit.responseBodyConverter<Any>(rightType, ANNOTATIONS)

        val statusCode = annotated(annotations)

        return EitherCallAdapter(left, right, statusCode, handler)

    }

    companion object {
        fun create(): EitherCallAdapterFactory {
            return create(Handler(Looper.getMainLooper()))
        }

        fun create(handler: Handler): EitherCallAdapterFactory {
            return EitherCallAdapterFactory(handler)
        }
    }

    private fun annotated(annotations: Array<Annotation>): InvocationPolicy {
        for (annotation in annotations) {
            if (annotation is InvocationPolicy) {
                return annotation
            }
        }

        return object : InvocationPolicy {

            override fun left(): IntArray {
                return IntArray(0)
            }

            override fun right(): IntArray {
                return IntArray(0)
            }

            override fun leftRange(): Array<InvocationPolicy.StatusCodeRange> {
                return arrayOf(InvocationPolicy.StatusCodeRange.SUCCESS, InvocationPolicy.StatusCodeRange.REDIRECT)
            }

            override fun rightRange(): Array<InvocationPolicy.StatusCodeRange> {
                return arrayOf(InvocationPolicy.StatusCodeRange.CLIENT_ERROR, InvocationPolicy.StatusCodeRange.SERVER_ERROR)
            }
        }
    }

    inner class EitherCallAdapter<L, R> constructor(private val left: Converter<ResponseBody, L>,
                                                    private val right: Converter<ResponseBody, R>,
                                                    private val statusCode: InvocationPolicy,
                                                    private val handler: Handler) : CallAdapter<ResponseBody, EitherCall<L, R>> {

        override fun responseType(): Type {
            return ResponseBody::class.java
        }

        override fun adapt(call: Call<ResponseBody>): EitherCall<L, R> {
            return EitherCall(call, left, right, statusCode, handler)
        }
    }
}