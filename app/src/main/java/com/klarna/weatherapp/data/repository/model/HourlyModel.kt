package com.klarna.weatherapp.data.repository.model

data class HourlyModel(
    val summary: String,
    val icon: String,
    val data: List<ForecastItemModel?>
)