package com.klarna.weatherapp.data.mapper

import com.klarna.weatherapp.data.api.dto.response.PlaceForecastResponse
import com.klarna.weatherapp.data.repository.model.*

object DtoModelMapper {

    fun mapForecast(forecasetResponse: PlaceForecastResponse): ForecastModel {
        return ForecastModel(
            forecasetResponse.latitude,
            forecasetResponse.longitude,
            forecasetResponse.timezone,
            mapForecastItem(forecasetResponse.currently),
            mapHourly(forecasetResponse.hourly)
        )
    }

    fun mapForecastItem(forecastItem: com.klarna.weatherapp.data.api.dto.response.ForecastItem): ForecastItemModel {
        return ForecastItemModel(
            forecastItem.time,
            forecastItem.summary,
            forecastItem.icon,
            forecastItem.precipIntensity,
            forecastItem.precipProbability,
            forecastItem.temperature,
            forecastItem.apparentTemperature,
            forecastItem.dewPoint,
            forecastItem.humidity,
            forecastItem.pressure,
            forecastItem.windSpeed,
            forecastItem.windGust,
            forecastItem.windBearing,
            forecastItem.cloudCover,
            forecastItem.uvIndex,
            forecastItem.visibility,
            forecastItem.ozone
        )
    }

    fun mapHourly(hourly: com.klarna.weatherapp.data.api.dto.response.Hourly): HourlyModel {
        return HourlyModel(hourly.summary,
            hourly.icon,
            hourly.data.map { mapForecastItem(it) }
        )
    }
}