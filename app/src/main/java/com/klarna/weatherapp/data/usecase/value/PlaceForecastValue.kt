package com.klarna.weatherapp.data.usecase.value

class PlaceForecastValue(val latitude: Double, val longitude: Double)