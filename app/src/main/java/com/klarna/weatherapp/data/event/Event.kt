package com.klarna.weatherapp.data.event

import com.klarna.weatherapp.error.WeatherError

sealed class Event<T>
data class SuccessEvent<T>(val data: T?) : Event<T>()
class LoadingEvent<T> : Event<T>()
data class ErrorEvent<T>(val error: WeatherError): Event<T>()

