package com.klarna.weatherapp.data.api.dto.response

data class PlaceForecastResponse(
    val latitude: Double,
    val longitude: Double,
    val timezone: String,
    val currently: ForecastItem,
    val hourly: Hourly
)

data class Hourly(
    val summary: String,
    val icon: String,
    val data: List<ForecastItem>
)

data class ForecastItem(
    val time: Long,
    val summary: String,
    val icon: String,
    val precipIntensity: Double,
    val precipProbability: Double,
    val temperature: Double,
    val apparentTemperature: Double,
    val dewPoint: Double,
    val humidity: Double,
    val pressure: Double,
    val windSpeed: Double,
    val windGust: Double,
    val windBearing: Double,
    val cloudCover: Double,
    val uvIndex: Double,
    val visibility: Double,
    val ozone: Double
)


