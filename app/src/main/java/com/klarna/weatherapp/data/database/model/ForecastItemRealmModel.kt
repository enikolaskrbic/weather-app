package com.klarna.weatherapp.data.database.model

import io.realm.RealmObject
import io.realm.annotations.RealmClass

@RealmClass
open class ForecastItemRealmModel(
    var time: Long = 1L,
    var summary: String = "",
    var icon: String = "",
    var precipIntensity: Double = 1.0,
    var precipProbability: Double = 1.0,
    var temperature: Double = 1.0,
    var apparentTemperature: Double = 1.0,
    var dewPoint: Double = 1.0,
    var humidity: Double = 1.0,
    var pressure: Double = 1.0,
    var windSpeed: Double = 1.0,
    var windGust: Double = 1.0,
    var windBearing: Double = 1.0,
    var cloudCover: Double = 1.0,
    var uvIndex: Double = 1.0,
    var visibility: Double = 1.0,
    var ozone: Double = 1.0
) : RealmObject()