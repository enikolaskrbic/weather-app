package com.klarna.weatherapp.data.repository.remote

import com.klarna.weatherapp.data.api.GlobalUnitCallback
import com.klarna.weatherapp.data.api.WeatherApi
import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.data.event.SuccessEvent
import com.klarna.weatherapp.data.mapper.DtoModelMapper
import com.klarna.weatherapp.data.repository.WeatherDataSource
import com.klarna.weatherapp.data.repository.model.ForecastModel
import com.klarna.weatherapp.data.usecase.value.ForecastValue
import com.klarna.weatherapp.data.usecase.value.PlaceForecastValue
import com.klarna.weatherapp.util.SingletonHolder
import io.reactivex.Flowable
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred

class WeatherRemoteDataSource(private val mWeatherApi: WeatherApi): WeatherDataSource {

    companion object : SingletonHolder<WeatherRemoteDataSource, WeatherApi>(::WeatherRemoteDataSource)

    override fun saveWeather(forecastValue: ForecastValue): Deferred<Event<Unit>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun fetchWeather(placeForecastValue: PlaceForecastValue): Deferred<Event<ForecastModel>> {
        val completableDeferred = CompletableDeferred<Event<ForecastModel>>()
        mWeatherApi.fetchWeather(placeForecastValue.latitude, placeForecastValue.longitude).callback(GlobalUnitCallback(completableDeferred) {
            it?.let {
                completableDeferred.complete(SuccessEvent(DtoModelMapper.mapForecast(it)))
            }
        })


        return completableDeferred
    }

    override fun getWeather(): Flowable<Event<List<ForecastModel?>>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}