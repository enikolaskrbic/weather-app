package com.klarna.weatherapp.data.repository.annotation

import android.app.Application

interface ApplicationScope {
    val application: Application
}