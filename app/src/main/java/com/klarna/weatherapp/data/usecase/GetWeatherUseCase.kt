package com.klarna.weatherapp.data.usecase

import com.klarna.weatherapp.data.event.Event
import com.klarna.weatherapp.data.repository.WeatherRepository
import com.klarna.weatherapp.data.repository.model.ForecastModel
import io.reactivex.Flowable

class GetWeatherUseCase: UseCase<Unit,Flowable<Event<List<ForecastModel?>>>> {

    private val mWeatherRepository = WeatherRepository.getInstance()

    override fun invoke(value: Unit): Flowable<Event<List<ForecastModel?>>> {
        return mWeatherRepository.getWeather()
    }
}