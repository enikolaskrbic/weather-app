package com.klarna.weatherapp.data.api

import com.klarna.weatherapp.data.api.dto.response.PlaceForecastResponse
import com.klarna.weatherapp.data.api.retrofit.EitherCall
import com.klarna.weatherapp.error.ErrorResponse
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path

interface WeatherApi {
    @GET("2bb07c3bece89caf533ac9a5d23d8417/{latitude},{longitude}")
    fun fetchWeather(@Path("latitude") latitude: Double, @Path("longitude") longitude: Double): EitherCall<PlaceForecastResponse, ErrorResponse>
}