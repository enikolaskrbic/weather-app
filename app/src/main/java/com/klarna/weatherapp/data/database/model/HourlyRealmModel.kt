package com.klarna.weatherapp.data.database.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.RealmClass

@RealmClass
open class HourlyRealmModel(
    var summary: String = "",
    var icon: String = "",
    var data: RealmList<ForecastItemRealmModel?> = RealmList()
) : RealmObject()