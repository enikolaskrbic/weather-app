package com.klarna.weatherapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.klarna.weatherapp.navigation.NavigationController

class WeatherActivity : AppCompatActivity() {

    private lateinit var mNavigationController: NavigationController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)
        mNavigationController = NavigationController(this)
        mNavigationController.navigateToHome()
    }

}
